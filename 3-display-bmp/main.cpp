#include <iostream>
#include <SDL2/SDL.h>

int main(int argc, char *argv[]) {
	SDL_Window *window=			0;	// window made on heap -- need to make sure we have enough memory when launching, so we don't put it on stack

	// With SDL_Surface, it's like a canvas. We'll load an image onto imageSurface and "paint" it to windowSurface
	SDL_Surface *windowSurface=	0;	// SDL_surface like a canvas, which is why we see white background on it by default
	SDL_Surface *imageSurface=	0;	// another canvas it seems

	if(SDL_Init(SDL_INIT_VIDEO) < 0)
		std::cout << "Video initialization error: " << SDL_GetError() << std::endl;

	const char *title=		"SDL CodingMadeEasy Series";
	const int screenWidth=	640;
	const int screenHeight=	480;
	window=					SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, SDL_WINDOW_SHOWN);
	if(window == NULL)
		std::cout << "Window creation error: " << SDL_GetError() << std::endl;

	// If window made, here we go...
	windowSurface=			SDL_GetWindowSurface(window);	// We want to get surface of `window`
	const char *bmp=		"Hello.bmp";
	imageSurface=			SDL_LoadBMP(bmp);				// Will return SDL_surface, or NULL on error
	if(imageSurface == NULL)
		std::cout << "Image loading error: " << SDL_GetError() << std::endl;

	// blitting -- aka take one surface, draw it to another surface
	SDL_BlitSurface(imageSurface,NULL,windowSurface,NULL);	// take from image surface and display on windowSurface

	// when we make window, nothing actually happens, we must run this
	SDL_UpdateWindowSurface(window);	// Remember, we must update surface layer with content of `window`
	SDL_Delay(5000); 					// stop for X milliseconds

	// We made a pointer *window and allocated it to memory,so let's free that memory
	SDL_FreeSurface(imageSurface);
	imageSurface=	0;
	SDL_DestroyWindow(window);
	window=			0;
	windowSurface=	0;
	SDL_Quit();

	return 0;
}
